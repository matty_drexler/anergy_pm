# general idea for how a job needs to be set up

from project.resources import *
from project.jobs import *
from project.generators import generate

norm = None

T1969 = Job('T1969', generate.default_sections(),  generate.default_resource_list())

T1969.add('A', '%AS4910%')
T1969.show('A')

T1969['A00'].estimate = norm.award_costs(value=1000)


