from project.resources import *
from collections import OrderedDict
from project.generators import generate

from project import wbs


class Scope(wbs.Element):
    def __init__(self, job, wbs, name, description=""):
        super().__init__(job, wbs, name, description)

    @staticmethod
    def type():
        return 'Scope'


class Task(wbs.Element):
    def __init__(self, job, wbs, name, description=""):
        super().__init__(job, wbs, name, description)

    @property
    def parent(self):
        return self._parent

    @parent.setter
    def parent(self, value):
        self._parent = value

    @staticmethod
    def type():
        return 'Task'


class Job(Blank):
    """Class which contains data about the job itself"""
    def __init__(self, number, name='', defaultResourceList=True, defaultSections=True):
        self.number = number
        self.name = name
        self.sections = generate.default_sections() if defaultSections else OrderedDict()
        self.scopes = OrderedDict()
        self.resourceList = generate.default_resource_list() if defaultResourceList else ResourceList()

    def __str__(self):
        return "<Job {}>".format(self.number)

    def __getitem__(self, item):
        pass

    @property
    def number(self):
        return self._number

    @number.setter
    def number(self, value):
        self._number = value

    # Section related methods
    def set_resource_list(self, resourceList):
        assert isinstance(resourceList, ResourceList)
        self.resourceList = resourceList

    def add_resource(self, resource):
        self.resourceList.add_resource(resource)

    def remove_resource(self, resourceName):
        self.resourceList.remove_resource(resourceName)

    def add_section(self, letter, detail):
        assert isinstance(letter, str) and len(letter)==1
        self.sections[letter] = detail

    def add_sections(self, details):
        for letter in list(details):
            self.add_section(letter, details[letter])

    def remove_section(self, letter):
        del self.sections[letter]

    def clear_sections(self):
        self.sections = OrderedDict()

    def output(self, verbose=False, scopes_only=True, summary_only=False):
        out = [("Job = {0:>6s}  {1:>40.40s}".format(self.number, self.name))]
        if summary_only and not verbose:
            out.append("  Number of Sections={0}".format(len(self.sections)))
            out.append("  Number of Scopes={0}".format(len(self.scopes)))
        if summary_only and verbose:
            map(out.append, ('  {0:<2.2s} : {1}'.format(*x) for x in self.sections))
            map(out.append, ('  {0:<2.2s} : {1}'.format(*x) for x in self.scopes))
        return out

    def add(self, wbs, add_string):
        """
        :param wbs: string with the node from which you will add
        :param add_string:
        :return:
        """

        return self

    def add_scope(self, *args):
        pass

    def add_scope_by_wbs(self, wbs, description=''):
        pass

    def add_scopes_by_string(self, scope_string):
        pass

    def remove_scope(self, wbs):
        pass

    def get(self, wbs):
        # for tasks and scopes
        pass

    def update(self, wbs):
        # for tasks and scopes
        pass

    def set(self, wbs, *args, **kwargs):
        # for tasks and scopes
        pass
