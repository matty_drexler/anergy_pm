from project.resources import *
from project.jobs import *
from collections import OrderedDict
import yaml


from os import path
with open(path.join(path.dirname(__file__), 'data/global.yaml'), 'r') as f:
    GLOBAL_YAML = yaml.load(f)


class Generators(Blank):
    @staticmethod
    def default_sections(style='full_list'):
        return OrderedDict(GLOBAL_YAML['sections'][style])

    @staticmethod
    def default_resource_list(style='general'):
        return OrderedDict(GLOBAL_YAML['resources'][style])


generate = Generators()


