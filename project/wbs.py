import re

from project import Blank
from networkx import DiGraph


class Token(Blank):
    regex = r"^(?P<section>[A-Z])(?P<scope>\d{2})(?:-(?P<subscope>\d{2}))?" + \
            "(?:\/(?P<task>\d{2})(?:[.-](?P<subtask>\d{2}))?)?$"

    def __init__(self, code):
        match = re.search(self.regex, code)
        if not match:
            raise ValueError("Cannot create WBS from invalid code :{}".format(code))
        else:
            self._code = code
            self._gr = match.groups()

    @property
    def wbs(self):
        return self._code

    @wbs.setter
    def wbs(self, newCode):
        self.__init__(newCode)

    @property
    def section(self):
        return self._gr[0]

    @property
    def scope(self):
        return self._gr[0] + self._gr[1]

    @property
    def subscope(self):
        return self._gr[2]

    @property
    def task(self):
        return self._gr[3]

    @property
    def subtask(self):
        return self._gr[4]

    @property
    def level(self):
        return max(l for l, v in enumerate(self._gr) if v is not None)

    @property
    def level_name(self):
        return ['section', 'scope', 'subscope', 'task', 'subtask'][self.level]

    def __repr__(self):
        return self._code

    def __str__(self):
        return self._code

    def __eq__(self, other):
        if isinstance(other, Token):
            other = other.wbs
        return self.wbs == other

    def prev(self, step=1):
        """
        Get the prev wbs code in the series
        :param step: decrease to find next default = 1
        :return: WBS code of previous in series
        """
        val = int(self._gr[self.level])
        if val > step:
            return Token(self._code[:-2] + str(val - step).zfill(2))

    def next(self, step=1):
        """
        Get the next wbs code in the series
        :param step: increase to find next default = 1
        :return: WBS code of next in series
        """
        val = int(self._gr[self.level])
        if val < 100-step:
            return Token(self._code[:-2] + str(val + step).zfill(2))

    @property
    def parent(self):
        """
        Determine the parent code from current code
        :return: WBS of parent code or None if at scope level
        """
        if self.level > 1:
            return Token(self._code[:-3])
        else:
            return None

    @property
    def child_scope(self):
        """
        Determines the 00th child if an additional scope is added
        Note: can only sub-scope scopes!
        :return: new WBS
        """
        if self.level == 1:
            return Token(self._code + '-00')
        else:
            return None

    @property
    def child_task(self):
        """
        Returns the 00th child code for a task added
        Note: can only task or sub-task but no further
        :return: new WBS
        """
        if self.level <= 2:
            return Token(self._code + "/00")
        elif self.level == 3:
            return Token(self._code + '-00')
        else:
            return None


class Element(Blank):
    def __init__(self, job, wbs, name='', description=''):
        self._code = Token(wbs)
        self._job = job
        self.name = name
        self.description = description
        self.children = None
        self.parent = None

    @property
    def wbs(self):
        return self._code

    @wbs.setter
    def wbs(self, wbs):
        self._code = wbs

    @property
    def job(self):
        return self._job

    def prev(self):
        return self

    def next(self):
        return self

    def get_parent(self):
        p = self.wbs.parent
        return self.job.get(p) if p else None

    def children(self):
        pass

    # I think this is in the wrong spot ... can only add through job
    # def add(self, detail):
    #     """
    #     Allows for multiple types of additions.
    #     :param detail:
    #     :return:
    #     """
    #     regex = re.compile(r'\W*([^,]+)')
    #     for match in regex.finditer(detail):
    #
    #     return matches


