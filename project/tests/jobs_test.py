import unittest
from project.jobs import Job
from project.wbs import Token
from project.generators import generate
from collections import OrderedDict
import yaml


class WBSCodeTestCase(unittest.TestCase):
    def test_WBS_properties(self):
        wbs = Token("D10-20/30-40")
        self.assertEqual(wbs.section, 'D')
        self.assertEqual(wbs.scope, 'D10')
        self.assertEqual(wbs.subscope, '20')
        self.assertEqual(wbs.task, '30')
        self.assertEqual(wbs.subtask, '40')
        self.assertEqual(wbs.level, 4)
        wbs.wbs = 'E10/20'
        self.assertEqual(wbs.section, 'E')
        self.assertEqual(wbs.scope, 'E10')
        self.assertEqual(wbs.subscope, None)
        self.assertEqual(wbs.task, '20')
        self.assertEqual(wbs.subtask, None)
        self.assertEqual(wbs.level, 3)
        wbs.wbs = 'E10'
        self.assertEqual(wbs.level, 1)
        wbs.wbs = 'E10-20'
        self.assertEqual(wbs.level, 2)

    def test_WBS_forms(self):
        self.assertRaises(ValueError, Token, "D")
        self.assertTrue(Token("D10"))
        self.assertTrue(Token("D10/10"))
        self.assertTrue(Token('D10/10-10'))
        self.assertTrue(Token('D10-10'))
        self.assertTrue(Token('D10-10/10'))
        self.assertTrue(Token('D10-10/10-10'))

    def test_WBS_magic_methods(self):
        wbs = Token("D10-20/30-40")
        self.assertEqual(wbs.__str__(), 'D10-20/30-40')
        self.assertEqual(wbs.__repr__(), 'D10-20/30-40')

    def test_WBS_next_prev(self):
        self.assertEqual(Token("D20").next(step=5), 'D25')
        self.assertEqual(Token("D20-20").next(step=5), 'D20-25')
        self.assertEqual(Token("D20-20/39").prev(step=9), 'D20-20/30')
        self.assertEqual(Token("D20/40-00").prev(), None)

    def test_WBS_child_parent(self):
        self.assertEqual(Token('E40/40').parent, 'E40')
        wbs = Token('A00')
        self.assertEqual(wbs.child_scope, 'A00-00')
        self.assertEqual(wbs.child_task, 'A00/00')
        self.assertEqual(wbs.child_task.child_task, 'A00/00-00')
        self.assertEqual(wbs.child_scope.child_task, 'A00-00/00')
        self.assertEqual(wbs.child_scope.child_task.child_task, 'A00-00/00-00')


class JobsCreationTestCase(unittest.TestCase):
    def setUp(self):
        self.T1969 = Job("T1969")
        self.job = Job("job")

    def test_number(self):
        self.assertEqual(self.T1969.number, "T1969")
        self.assertEqual(self.job.number, "job")
        self.job.number = "T1555"
        self.assertEqual(self.job.number, "T1555")


class JobsSectionsTestCase(unittest.TestCase):
    def setUp(self):
        self.T1969 = Job("T1969")
        self.T1969.add_sections(generate.default_sections())

    def test_add_section(self):
        self.T1969.add_section("A", "alphabet")
        self.assertEqual(self.T1969.sections['A'], 'alphabet')

    def test_add_sections(self):
        # self.T1969.add_sections(generate.standard_sections())
        self.assertEqual(''.join(k for k in self.T1969.sections), 'ADMCSPEGFH')

    def test_remove_sections(self):
        # self.T1969.add_sections(generate.standard_sections())
        self.T1969.remove_section("C")
        self.assertEqual(''.join(k for k in self.T1969.sections), 'ADMSPEGFH')

    def test_clear_sections(self):
        self.T1969.clear_sections()
        self.assertEqual(len(self.T1969.sections), 0)




if __name__ == '__main__':
    unittest.main()
