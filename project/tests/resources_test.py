import unittest
import yaml

from project.resources import *


class ResourceTestCase(unittest.TestCase):

    def test_work_resource(self):
        engineer = Work("Engineer")
        self.assertEqual(engineer.name, "Engineer")
        self.assertEqual(engineer.cost, 0.0, "cost")
        self.assertEqual(engineer.rate, 0.0)
        engineer.rate = 55.0
        self.assertEqual(engineer.cost, 55.0)
        self.assertEqual(engineer.rate, 55.0)
        engineer.cost = 550.0
        self.assertEqual(engineer.cost, 55.0)
        self.assertEqual(engineer.rate, 55.0)
        engineer.location = "Bunbury"
        self.assertEqual(engineer.location, "Bunbury")
        self.assertEqual(str(engineer), "Engineer ($55.00/hr)")
        self.assertEqual(repr(engineer), "<Work(Engineer)>")
        engineer = Work("Engineer", 55.0, "Bunbury")

    def test_cost_resource(self):
        pump = Cost("Pump")
        self.assertEqual(pump.name, "Pump")
        self.assertEqual(pump.cost, 0.0, "cost")
        pump.cost = 550.0
        self.assertEqual(pump.cost, 550.0)
        pump.location = "Bunbury"
        self.assertEqual(pump.location, "Bunbury")
        self.assertEqual(str(pump), "Pump ($550.00)")
        self.assertEqual(repr(pump), "<Cost(Pump)>")

    def test_material_resource(self):
        steel = Material("Steel")
        self.assertEqual(steel.name, "Steel")
        self.assertEqual(steel.cost, 0.0, "cost")
        self.assertEqual(steel.rate, 0.0)
        steel.rate = 1.50
        steel.unit = "kg"
        self.assertEqual(steel.cost, 1.50)
        self.assertEqual(steel.rate, 1.50)
        steel.cost = 550.0
        self.assertEqual(steel.cost, 1.50)
        self.assertEqual(steel.rate, 1.50)
        steel.location = "Perth"
        self.assertEqual(steel.location, "Perth")
        self.assertEqual(str(steel), "Steel ($1.50/kg)")
        self.assertEqual(repr(steel), "<Material(Steel)>")


class ResourceListTestCase(unittest.TestCase):

    def setUp(self):
        self.resources = ResourceList()

    def test_load_from_yaml(self):
        raw = """
Work:
  - { name: welder, location: Bunbury, rate: 90.00 }
  - { name: painter, location: Bunbury, rate: 90.00 }
  - { name: storeman, location: Bunbury, rate: 90.00 }
  - { name: fitter, location: Bunbury, rate: 100.00 }
Cost:
  - { name: pump, location: Perth, cost: 400.00 }
  - { name: fan, location: Perth, cost: 250.00 }
Material:
  - { name: steel, location: Bunbury, unit: kg, rate: 1.75 }
  - { name: 316_ss, location: Bunbury, unit: kg, rate: 4.75 }         
        """
        self.resources = ResourceList()
        self.resources.from_yaml(raw)
        self.assertEqual(self.resources.count(), 8)

    def test_add_resource(self):
        self.assertRaises(TypeError, self.resources.add_resource, "This is not a valid resource")
        self.resources.add_resource(Work("engineer"))
        self.assertRaises(KeyError, self.resources.add_resource, Work("engineer"))


class ResourceUseTestCase(unittest.TestCase):

    def test_resource_use_work(self):
        engineer = Work("Engineer", 100.0, "Bunbury")
        est1 = ResourceUse(engineer, 4)
        # self.assertEqual(est1.cost, 400.0)



if __name__ == '__main__':
    unittest.main()
