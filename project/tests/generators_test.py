import unittest
from project.resources import *
from project.jobs import *
from project.generators import generate


class GenerateSections(unittest.TestCase):
    def test_standard_sections(self):
        job = Job('myJob')
        job.add_sections(generate.default_sections())
        self.assertEqual(''.join(k for k in job.sections), 'ADMCSPEGFH')


if __name__ == '__main__':
    unittest.main()
