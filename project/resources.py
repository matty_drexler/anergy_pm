from project.currency import AUDstr
from project import *
import yaml

"""
Resource Module

Assumptions:
  - all rates and costs are in AUD
  
  
  TODO: Add _repr_html_ method for IPython work
  TODO: Allow nesting to occur for resources
"""


class Resource(Blank):
    """ A resource is a """

    def __init__(self, name, cost=0.0, location="", parent=None):
        self.location = location
        self.name = name
        self.cost = cost
        self.parent = parent
        self.currency = "AUD"

    @property
    def type(self):
        return self.__class__.__name__

    def value(self, qty=1, override_cost=None):
        return qty * (self.cost if not override_cost else override_cost)

    @property
    def cost(self):
        return self._cost

    @cost.setter
    def cost(self, value):
        # Allow the interpretation of either a string with $x,xxx.xx etc or as an actual number
        self._cost = value

    # @property
    # def parent(self):
    #     return self._parent
    #
    # @parent.setter
    # def parent(self, resource):
    #     """Argument"""
    #     # Perform a check to make sure not it's own parent
    #     # recursive until find itself or None or a previously named member
    #     # set parent to resource if None else raise an Error

    def __str__(self):
        return "{} ({})".format(self.name, AUDstr(self.cost))

    def __repr__(self):
        return "<{}({})>".format(self.__class__.__name__, self.name)


class Cost(Resource):
    """Specifically a cost resource"""

    def __init__(self, name, cost=0.0, location="", parent=None):
        super().__init__(name, cost, location, parent)


class Work(Resource):
    """Specifically a labour resource"""

    def __init__(self, name, rate=0.0, location="", parent=None):
        self.rate = rate
        self.unit = "hr"
        super().__init__(name, self.cost, location, parent)

    @property
    def rate(self):
        return self._rate

    @rate.setter
    def rate(self, value):
        self._rate = value

    @property
    def cost(self):
        return self.rate

    @cost.setter
    def cost(self, value):
        pass

    def __str__(self):
        return "{} ({}/hr)".format(self.name, AUDstr(self.cost))


class Material(Resource):
    """Specifically a material resource"""

    def __init__(self, name, rate=0.0, unit="", location="", parent=None):
        self.rate = rate
        self.unit = unit
        super().__init__(name, self.cost, location, parent)

    @property
    def unit(self):
        return self._unit

    @unit.setter
    def unit(self, unit):
        self._unit = unit

    @property
    def rate(self):
        return self._rate

    @rate.setter
    def rate(self, value):
        self._rate = value

    @property
    def cost(self):
        return self.rate

    @cost.setter
    def cost(self, value):
        pass

    def __str__(self):
        return "{} ({}/{})".format(self.name, AUDstr(self.cost), self.unit)


class ResourceList(Blank):
    """
    A list of resources that can be accessed.
    - list - complete array
    """
    resourceClasses = [Work, Cost, Material]
    resourceTypes = [c.__name__ for c in resourceClasses]

    def __init__(self):
        self._list = []

    def __repr__(self):
        pass

    def __str__(self):
        pass

    def __getitem__(self, item):
        pass

    def __setitem__(self, key, value):
        pass

    def __add__(self, other):
        pass

    def __iter__(self):
        return self._list

    def count(self):
        return len(self._list)

    def add_resource(self, resource):
        if not isinstance(resource, Resource):
            raise TypeError("add_resource requires a Resource object")
        if resource.name in self.get_resource_names_list():
            raise KeyError("add_resource cannot add a resource with the same name as existing resource")
        self._list.append(resource)

    def add_resources(self, resources):
        if not isinstance(resources, (list, tuple)):
            raise TypeError("add_resources requires a list or tuple of resources")
        for resource in resources:
            self.add_resource(resource)

    def remove_resource(self, resourceName):
        pass

    def merge_list(self, resourceList):
        pass

    def get_resource(self, resourceName):
        r = [item for item in self._list if item.name == resourceName]
        return r[0] if len(r) > 0 else None

    def get_resource_by_index(self, index):
        return self._list[index]

    def get_resource_index(self, resourceName):
        try:
            idx = self.get_resource_names_list().index(resourceName)
        except IndexError:
            idx = None
        return idx

    def has_resource(self, resourceName):
        r = [item for item in self._list if item.name == resourceName]
        return len(r) > 0

    def get_resource_names_list(self):
        return [item.name for item in self._list]

    def from_yaml(self, yamlString):
        # Assumes a yaml string given to system to process
        data = yaml.load(yamlString)
        for cls in (c for c in self.resourceClasses if c.__name__ in data.keys()):
            self.add_resources([cls(**res) for res in data[cls.__name__]])


class ResourceUse(Blank):
    """
    This class contains a resource along with a quantity. It is created through the use_resource method
    of the ResourceList class or the use method of the resource class
    """
    def __init__(self, resource, qty=1, override_cost=None, override_rate=None):
        assert isinstance(resource, Resource)
        assert isinstance(qty, (float, int))
        self.resource = resource
        self.qty = qty
        self.override_cost = override_cost
        self.override_rate = override_rate

    @property
    def qty(self):
        return self._qty

    @qty.setter
    def qty(self, qty):
        self._qty = qty

    @property
    def cost(self):
        return self.override_cost if self.override_cost else self.resource.cost

    @property
    def value(self):
        pass
