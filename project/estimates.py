

from project.resources import *


class Estimate(Blank):
    """

    """
    def __init__(self, name, description="", resources=None):
        self.name = name
        self.description = description
        self.add_resources(resources)

    def __add__(self, other):
        pass

    def __mul__(self, other):
        pass

    def __str__(self):
        pass

    def __repr__(self):
        pass

    def __getitem__(self, item):
        pass

    def __setitem__(self, key, value):
        pass

    def add_resourceUse(self, resourceUse):
        """A resource use is a string in the pattern name[qty]
                -> name will then be applied to a resource name
                -> qty will then be applied to make the quantity
        Multiple uses can be in one string separated by a comma
        """
        pass

    def get_total_cost(self):
        pass

    def get_resource_costs(self):
        pass



