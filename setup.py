import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="anergy_pm",
    version="0.0.1",
    author="Matty Drexler",
    author_email="matty.drexler@serious-monkey-business.com",
    description="Anergy Python Project Management Tools",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/matty_drexler/anergy_pm",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
